﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockInteraction : InteractableObject {

    public override void Interact(GameObject obj)
    {
        if(obj.name == "Key")
        {
            MyDebug.instance.SetText("opening a wall?!?");
            Destroy(gameObject);
            
        }
        else
        {
            base.Interact(obj);
        }
    }

}
