﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongBoxInteraction : InteractableObject {

    public Transform player;
    private Rigidbody2D m_rb;
    public float speed = 1;
    public float angle = 0;

    private void Start()
    {
        m_rb = this.GetComponent<Rigidbody2D>();
    }


    public override void Interact()
    {
        MyDebug.instance.SetText("keep pushing");
        Quaternion rotation = this.transform.rotation;

        int direction = 1;
        if (player.position.x < this.transform.position.x)
        {
            direction = -1;   
        }

        m_rb.AddTorque(speed * direction);
        
    }


}
