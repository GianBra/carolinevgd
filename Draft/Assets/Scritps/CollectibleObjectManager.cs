﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CollectibleObjectManager : MonoBehaviour {
    [System.Serializable]
    private struct Collectible
    {
        public string name;
        public Button btn;
        public GameObject obj;
        public bool equipped;
        public bool pickedUp;
    }

    [SerializeField]
    private Collectible[] objects;

    public Transform player;
    public GameObject equippedObj { get; private set; }
    public string equippedName { get; private set; }

    public static CollectibleObjectManager instance;

	// Use this for initialization
	void Awake () {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }else if(instance != this)
        {
            Destroy(gameObject);
        }
	}

    private void Start()
    {
        for (int i = 0; i < objects.Length; i++)
        {
            
            objects[i].btn.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {
		if(equippedObj != null)
        {
            equippedObj.transform.position = player.transform.position;
        }
	}

    public void PickUp(string name)
    {
         for(int i = 0; i< objects.Length; i++)
        {
            if (objects[i].name == name)
            {
                objects[i].obj.SetActive(false);
                objects[i].btn.gameObject.SetActive(true);
                objects[i].btn.GetComponentInChildren<Text>().text = "equip " + objects[i].name;
                objects[i].pickedUp = true;
            }
        }
    }

    public void Equip(string name)
    {
        string objName = "";
        if(equippedObj!= null)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if(objects[i].equipped == true)
                {
                    objects[i].btn.GetComponentInChildren<Text>().text = "equip " + objects[i].name;
                    objects[i].obj.SetActive(false);
                    objects[i].equipped = false;
                }
            }
            objName = equippedName;
            equippedName = null;
            equippedObj = null;
        }


        if (objName != name)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if(objects[i].name == name)
                {
                    objects[i].btn.GetComponentInChildren<Text>().text = "unequip " + objects[i].name;
                    objects[i].obj.SetActive(true);
                    objects[i].equipped = true;
                    objects[i].obj.transform.position = player.transform.position;
                    equippedName = objects[i].name;
                    equippedObj = objects[i].obj;
                }
            }
        }
    }



}
