﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private InteractableObject touchedObject=null;
    private bool interacting=false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.LeftShift) && touchedObject != null)
        {
           if (CollectibleObjectManager.instance.equippedObj != null)
            {
               touchedObject.Interact(CollectibleObjectManager.instance.equippedObj);
            }
            else {
                touchedObject.Interact();
            }
            interacting = true;
        }
        if(Input.GetKeyUp(KeyCode.LeftShift) && touchedObject != null)
        {
            touchedObject.InteractionEnd();
            interacting = false;
            
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        InteractableObject obj = collision.gameObject.GetComponent<InteractableObject>();
        if (obj != null)
        {
            touchedObject = obj;
            MyDebug.instance.SetText("Collided");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        InteractableObject obj = collision.gameObject.GetComponent<InteractableObject>();
        if (obj != null && obj == touchedObject)
        {
            if (interacting)
            {
                touchedObject.InteractionEnd();
                interacting = false;
            }
            touchedObject = null;
            MyDebug.instance.SetText("Left collider");
        }
    }





}
