﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MyDebug : MonoBehaviour {

    private Text text;
    public float updateTime = 2f;
    private float remainingTime;
    public static MyDebug instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(this);
    }

    // Use this for initialization
    void Start () {
        remainingTime = updateTime;
        text = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (remainingTime < 0)
            text.text = "ZzZzZ";
        remainingTime -= Time.deltaTime;
	}

    public void SetText(string s)
    {
        text.text = s;
        remainingTime = updateTime;
    }

}
