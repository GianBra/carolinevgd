﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalInteraction : InteractableObject {

    public GameObject thinking;
    private bool haveEat = false;
    private bool move = false;
    private Vector3 destination;

    public float speed;

    private void Start()
    {
        destination = new Vector3(transform.position.x, transform.position.y + 10, transform.position.z);
    }


    private void Update()
    {
        if (move)
        {
            transform.position = Vector3.Lerp(transform.position, destination, speed * Time.deltaTime);
            if (transform.position.y >= destination.y)
                move = false;
        }
    }


    public override void Interact(GameObject obj)
    {
        if (obj.name == "Food")
        {

            MyDebug.instance.SetText("GNAM GNAM");
            haveEat = true;
            move = true;
            thinking.SetActive(false);
        }
        else
            base.Interact(obj);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !haveEat)
            thinking.SetActive(true);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            thinking.SetActive(false);
    }
}
