﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxInteraction : InteractableObject {

    public Transform target;
    public float m_MaxSpeed;
    bool first=true;
    private Vector2 m_diffPosition;
    private Transform m_transform;
  
    void Start()
    {
        m_transform = this.GetComponent<Transform>();
      
    }

    public override void Interact()
    {
        MyDebug.instance.SetText("this is heavy");
        if (first)
        {
            m_diffPosition = target.position - m_transform.position;
            first = false;
        }
        float actual = target.position.x - m_transform.position.x;

        if (Mathf.Abs(actual) > Mathf.Abs(m_diffPosition.x)) {
            float difference = actual - m_diffPosition.x;
            this.transform.position = new Vector2(transform.position.x + difference, transform.position.y);
        }   


    }

    public override void InteractionEnd()
    {
        MyDebug.instance.SetText("here's ok");
        first = true;
    }
}
