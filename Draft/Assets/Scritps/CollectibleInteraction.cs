﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleInteraction : InteractableObject {

    public string m_ObjectName;

    public override void Interact()
    {
        MyDebug.instance.SetText("Nice! " + m_ObjectName + "!");
        CollectibleObjectManager.instance.PickUp(m_ObjectName);
        Destroy(gameObject.GetComponent<BoxCollider2D>());
        Destroy(this);
    }


}
