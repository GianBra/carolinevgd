﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour {
    
    public virtual void Interact()
    {
        MyDebug.instance.SetText(gameObject.name + ": nothing happens");
    }
    public virtual void InteractionEnd() { }

    public virtual void Interact(GameObject obj)
    {
        MyDebug.instance.SetText(gameObject.name + ": nothing happens");
    }

    public virtual void InteractionEnd(GameObject obj) { }
}
