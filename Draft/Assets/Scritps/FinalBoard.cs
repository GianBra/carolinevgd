﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBoard : MonoBehaviour {

    public GameObject other;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            other.SetActive(true);
            Destroy(gameObject);
        }
    }
}
