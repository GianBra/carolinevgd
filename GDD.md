#CAROLINE'S JOURNEY
## Game design document
### Overview
Caroline's Journey is a pacifist 2D platform where the player progress by helping creatures instead of fighting enemies.  
The user is going to experience something like a playable tale, with even a morale at the ending.
The concept in which you help creatures rather than fight is what makes this game unique. It differs from "Point and click" games because the puzzles also require exploration and platforming. However, the mechanics are simple: Caroline can do few basic actions that can be applied in many ways and in many situations.
The game is set in a dreamy, colourful world, populated by fantasy creatures, but all of them are friendly.

### Audience
The game targets everyone, but mostly children. Since there are no dialogs written in the game, minimal localisation is required.  
The game is developed initially for PC, but since it will fully support controllers it can be extended to consoles later on.  
Also the system requirements are minimal and no particular hardware is required to play.


### Suggested App descriptions
Short:
This is a kid's puzzle game, you have to help lost Caroline find her way home.

Long:
Caroline's Journey is an exploration and puzzle game for kids or kids at heart.

This game teaches the lessons of friendship and helping people in need to the kids.

Caroline fell through the hole into the darkness, away from her parents and her home. She is lost and needs to find her way back home. In this exploration game, in each level, you go through a different wild environment like Cave, Forest or mountains and help magical creatures in your adventure by solving small puzzles. While exploring the whole level, you will find many mysterious objects which will help you aid in your adventure, in the same level or in a future level. In return for your help, they help you back to pass the level and thus reaching your home. 

Features of the game:
Help the creatures to pass the level
Collect magical objects to aid your adventure
More creatures you help more points you earn
Unlock different environment each time you complete one level

Because at last "A friend in need is a friend indeed".

### Gameplay

#### How the game works
Caroline is supposed to go from the start and end of the stage, indicated by a sign in every stage. There will obstacles blocking the way. The goal is to solve the puzzles in the levels to remove the obstacles. The puzzles are solved with clever interaction with the environment and with the NPCs.

#### Controls
* The game is designed to be played with a controller, but it will also support the keyboard. (...) is the key for the controller, [...] for the keyboard. Mouse is not required to play.  

*  for interactables, when Caroline is near, a hint to press (A) appear. If pressed, if is an object it goes into the inventory, if is a NPC it appears a balloon explaining the problem.

| Action | Key (keyboard)  | Key (joystick) |
| :----- | :------------- | :------ |
| move right/left | Right/left arrows | Left analog stick |
| Jump | up arrow | A |
| Interact | Z | X |
| Use Object | X | Y |
| Open Inventory | C | R1 |
| Pause | Esc | Start |
| [Inventory] Equip | X | X |
| [Inventory] Navigate | Arrows | Left analog stick |

#### UI
In the bottom part of the screen, on the left side there are the two main buttons to interact with the scene, and on the right part there is the inventory.  
In the Inventory there are all the object you picked up in the level, and the player can equip one of them.  
The inventory is always open, and when opened it gain focus.

#### Level design decisions
* Need to have __more than one puzzle to solve at a time__, to give freedom to the player. For instance, design a puzzle that require two sub-puzzle to be solved and merged together in order to progress.
* Can't go back to past levels to give a sense of progression.
* A sign with a home symbol indicates the way to proceed.  
* The first stage is inside a cave, a not friendly place, the player want to go out from it. The second stage is a nice forest, which is kind of a reward itself for completing the cave. The third stage is still to be decided.
* We will develop three stages to have at least 20 minutes of gameplay.
* Every Item must be used inside the level to progress.  
* No powerups for Caroline, just kept simple. The only powerups she is gonna have are her friends (Creatures she helped on her way).
* Remember to add some helped monster to came back in future levels.
* Remember also to add at least one monster to help that gives no rewards. This is done to add some extra morality to the game: will you help someone just because he is in trouble, even if you are not going to have anything back?

### Characters
![](png_files/Caroline_Standing.png)  
Caroline is a young child, while all the NPC are non-human creatures: they can look like in many way, depending how they fit in the level: from magical monster to simple animals. The final creatures design is still to be decided.

### World description
The first level is going to be a cave because is meant to be an uncomfortable place the player wants to go out to, to encourage progression. So the soundtrack is very soft and made only with ambient sounds.  
The second level, the forest, is a reward itself for completing the cave stage, because it's a more friendly and colourful place, with a joyful soundtrack.  

The following prototypes are just two building blocks and are not meant to represent the final version of a level. Actually they are only two puzzle sample, but if we manage to complete them and make them work in the game, then the big work is done, and only
final levels are meant to be bigger, with many puzzles, split in sub-puzzles and maybe optional ones.  
Each puzzle has an obstacle, a creature that can help Caroline, a problem of that creature, and a way for Caroline to help it.

####Tutorial (first level) prototype
The tutorial zone is meant to teach all the basis to the player. In order the player is required to:

* pass a rock (jump)
* pass a pit (jump)
* pass a larger pit (jump further by holding space)
* go up a cliff (impossible by yourself!)
* feed a monster that can elevate you up (help)
    * go up some platforms and a ladder (climb)
    * find some food (Interact)
    * bring the food to the monster (use the object)

#### Forest level

* Obstacle: the signs indicates to climb the mountain
* Helper: a mage can make a potion to grow a giant vine. But he misses his staff and his hat.
* The hat is in a box attached to a rope. To obtain it Caroline plays with a creature that gift her a boomerang and with it she can cut the rope.
* the staff is behind a sleeping creature. To wake him up Caroline help a bird to find his missing mate with a horn simulating his cry. Making the cry altogether they wake up the creature.

#### Mountain level
* Obstacle: arrived at the peak of the mountain, the home village can be seen in the distance, but Caroline needs to fly to reach it.
* Helper: a dragon is lying on the ground. Unfortunately he is ill and cold.
* How to help: to help him, Caroline should prepare a medicine. Another creature living in a house nearby can help; the potion require 4 ingredient:
    * a root that can be found easily outside the house
    * a flower that need to be cut with the boomerang
    * a fruit that needs to be caught by the bird from the forest (Caroline can call them with the horn)
    * some water that can be collected with a jar
* once the medicine is cooked, Caroline can bring it to the dragon. With the renewed strength, he can carry Caroline to home
* (optional) Caroline can also take some medicine to another creature to make him happy  

Once Caroline completed the quest, a cutscene starts (can be a sequence of images), showing Caroline flying on the dragon, landing, finding the village empty and start crying. Then, the creatures from the past levels appears, Johnny carries Caroline on his back, and in the end they are happy altogether.  

### Story
The story is simple and has no modification from the concept document:

At the beginning you find yourself lost in an unknown place, alone and with no idea of what happened. You just want to go home, but the way home is not easy to go through. You have to rely on the help of many creatures to progress.  
Level after level you get more friends that give you more rewards and some creatures come back from other levels to help you, if you helped them when you could.  
At the end of your journey you will not find your home, you are in a dead end and alone, again. But then in the final cutscene, all the creatures you helped enter the scene, someone carries you on his back and you are happy again, realising that you already have a home, and you built that yourself during the journey.

## Assets needed for the game
### Core Sprites
All items, if layered, should have separated layer such that they can be sliced easily in unity.  

| Name    | Dimensions     | Done
| :------ | :------------- | :---
| Caroline's front | 150x300 | Y
| Caroline's back | 150x300 | Y
| Carolines's side | 150x300 | Y
| Bush | 400x400 | Y
| Backpack icon | 300x300 |
| Grass tile | 300x300 | Y
| Rock tile | 300x300 | Y
| [Hint] Pick up | 150x150 |
| [Hint] Speak | 150x150 |
| [Hint] Use object | 150x150 |
| XBOX 360 buttons | 100x100 | Y (P)
| Keyboard buttons | 100x100 | Y (P)
| Title game label | 800x400 |

### Tutorial Sprites

| Name | Dimensions  | Done
| :------------- | :--------- | :---
| Food  | 200x200 | Y
| Creature Side | 600x600 | Y
| Creature Front | 600x600 | Y
| Background | Pretty big | Y

### Forest sprites

| Name | Dimensions    | Done
| :------------- | :--------- | :---
| Bird | 150x150 | Y
| Sleeping creature  | 600x600 | Y
| Boomerang creature | 450x450 | Y
| Howl mage | 300x500 | Y
| Boomerang | 150x150 | Y
| Horn | 150x150 | Y
| Mage hat | 200x200 | Y
| Mage staff | 200x200 | Y
| Magic potion | 150x150 | Y
| Giant plant | 150x400 | Y
| Exit from the Cave | 1000x1000 |
| Trees | Big enough |

### Mountain Sprites
Assets for the environment are not in the table

| Name | Dimensions    | Done
| :------------- | :--------- | :---
| Turnip | 200x200 | Y
| Flower | 200x400 |
| Jar (empty) | 200x200 | Y
| Jar (water) | 200x200 | Y
| Jar (medicine) | 200x200 | Y
| Caldron (empty) | 400x400 | Y
| Caldron (with water) | 400x400 | Y
| Caldron (with medicine) | 400x400 | Y
| Cooking monster | 600x300 | Y
| Dragon | 800x800 | Y
| Ill optional creature | 300x300 |

| Messages (200x200) | Done    |
| :------- | :------- |
| [Dragon] I'm sick   |    
| [Cook] I need this 3 ingredient |
| [Cook] I need fire to cook something |
| [Cook] Take this medicine to the dragon |
| [Caroline] The dragon is sick, can you do something? |
| [Ill creature] Thank you!! |

| Cutscene images (1920x1080)| Done |
| :-------- | :------- |
| Caroline on the back of the dragon |
| Caroline flying on the dragon |
| C. landing in the village |
| C. sad finding no one in the village |
| C. crying |
| C. hearing something from outside the scene |
| All creatures she helped appears in the scene |
| Caroline and the creatures happy altogether |

Screen opens with just the logo caroline's journey and a subtilte Polimi Games Collective.
After a few fraction of seconds carloine enters with the dragon and flies around the logo and the background (Foresty background) fades in and we get final title screen with logo and foresty background which gives taste of game style.

Prologue Cut Scenes:
Caroline falls in from the hole and looks both ways. Walks a bit.
Voice over or text: 
I am lost.
I am scared.
Help me.
Help me Please.
Get me to my home.
I want o go home.


Game Starts.

Epilogue Cut Scenes:
Carline sees from mountain top a village in the distance.
Dragons comes and lands beside her. 
She gets on the dragon.
Dragon flies to the village.
Lands in the village. Looks around.
She sees a house.							Side View
Enters the house.							Back View
Noone. Empty House.
Empty House. Crying.							Front View
She hears a sound from outside.
She comes outside.
All the creatures are happy and outside waiting for her.		Side View
She walks to them.							Side View
Final image Carline in monsters. Happy.					Front View

"Home is where your people are."

## Sounds needed for the game

### Soundtracks
| Name     | Duration    | Done
| :------  | :---------- | :---
| Main theme | 2-3 minutes | Y
| Cave theme | 1-2 minutes |
| Forest theme | 2-3 minutes | Y
| Mountain theme | 2-3 minutes |

### SFX Core Sounds
| Name  | Duration  | Done
| :---- | :-------- | :---
| Caroline Jump | 0.5 sec |
| Caroline Walk | 0.5 sec | Y
| Caroline Climb | 0.5 sec |
| Navigation | 0.3 sec | Y
| Select | 0.3 sec | Y
| Cancel | 0.3 sec |
| Pick up Object | 0.3 sec | Y

### Tutorial SFX Sounds
| Name | Duration    | Done
| :------------- | :------------- | :---
| Creature eat    | 2 sec     | Y

### Forest SFX Sounds
| Name | Duration   | Done
| :------------- | :------------- |
| Bird cry     | 1 sec    | Y
| Sleeping Creature snort | 3 sec |
| Boomerang effect | 0.25 sec |

### Mountain SFX Sounds
TBA

## Animations needed for the game

### Core Animations
| Name  | Done    |
| :------------- | :------------- |
| Caroline idle  | Y     |
| Caroline jump | Y
| Caroline walking | Y
| Caroline eye blink | Y

### Tutorial animations
| Name | Done   |
| :-------- | :-------- |
| Creature idle  | Y
| Creature Lift player | Y
| Some animations for the environment |

### Forest Animations
| Name | Done   |
| :---------- | :--------- |
| Bird idle   | Y
| Bird worried | Y
| Bird make loud noise | Y
| Bird fly | Y
| Sleeping creature idle/snort | Y
| Sleeping creature wake up | Y
| Some animations for the environment |

### Mountain Animations
| Name | Done    |
| :---- | :------ |
| Cook idle  | Y
| Cook cooking | Y
| Dragon idle | Y
| Ill creature sad |
| Ill creature happy |
| Birds picking up the fruit |

## Scripts

### Core scripts
| Name | Done |
| :--- | :--- |
| DeadZoneCamera |  Y
| GameManager |  Y
| Save the object in the inventory | Y
| Player movements and animations |  Y
| Save and reload player's position | Y
| Go to the next level | Y
| Balloons to show when the player can interact | Y
| Manage Inventory | Y
| Player's ability to choose, equip and use items | Y

### Mountain Scripts
| Name | Done |
| :--- | :--- |
| Dragon  | Y
| Ill creature | Y
| Mole that cook the potion | Y
| Caldron | Y
| Horn that calls the birds | Y
| Flower | Y
| Jar empty/fill | Y

## What to do next (for 3/1/2018)

### Gian
- Initial cutscene
- Mushroom animation
- Assemble final cutscene
- Polish forest level and add checkpoints
- Poor creature in the last level (will be johnny recycled)

### Taran
- Add graphics in the mountain level
- Dragon fire breath Particle system
- Forest level owls magic particle system
- Stick fire PS
- Box breaking PS
- Plant growing PS
- Last level water

### Angelo
- Assemble the last level with all the script and objects
- Moving clouds and trees in the forest.
- Eventually move things in the mountain level
- Make the inventory semi-trasparent when not active

### Sayali

#### General

| What to do | Priority   |
| :------------- | :------------- |
| Mushroom sprite (cup and body) | High   |
| Title image for the game | Low |
| Final cutscene images | Medium |
| Messages without a grey circle around, but with actual balloons with it. Can be with various dimensions, some smaller, some bigger, based on how many things it needs to communicate | High |
| Smiles for the creature, indicating the status (happy/sad/I can help) | Low
| Hint for the interactions (take/talk) | Low

#### For the tutorial
| What to do | Priority   |
| :------------- | :------------- |
| Bigger background | Medium |
| Johhny happy face | Medium |
| Johhny sad/tired face | Medium |

#### For the Forest level
| What to do | Priority   |
| :------------- | :------------- |
| Bears arms with clear elbow | High |
| Blue bear (Miguel) complete sleeping face | Medium
| Blue/Orange bear complete face | Medium
| Box sprite | Medium
| Rope sprite | Medium
| A bigger tree sprite (trunk and foilage) | Medium

#### For the Mountain level
| What to do | Priority   |
| :------------- | :------------- |
| Messages as in the table | High |

## Deadlines

* January 3rd 2018: all the game working and deployed
* D4: presentation and all non-gameplay elements added (cutscenes, soundtracks, maybe backgrounds and details)


## Changelog
* Sunday 5 November 2017: initial version (Gian)
* Tuesday 7 November 2017: added thing done and to do (Gian)
* Saturday 11 November 2017: added world description (Gian)
* Sunday 12 November 2017: made a few corrections (Sayali), added a section for the assets (Gian)
* Tuesday 14 November 2017: regular weekly update based on the meeting (Gian)
* Sunday 19 November 2017: redesigned the forest level, added a small section for sfx sounds (Gian)
* Tuesday 27 November 2017: restructured the document with tables
* Friday 8 December 2017: simplified script table, added assets and info for the last level
* Tuesday 12 December 2017: update after the prototype presentation
* 26 December: update for the last deadline.
