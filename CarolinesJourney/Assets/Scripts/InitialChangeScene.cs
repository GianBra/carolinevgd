﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitialChangeScene : MonoBehaviour {

    public float timeToWait;
    public string sceneToLoad;

	[Header("Fade Effect")]
	public Texture texture;
	public float timeToFade = 1f;
	public float deadFadeTime = 1f;
	
	private float alpha;
	private bool fadingIn, fadingOut, keepBlack, deadFadeIn, deadFadeOut;
    

	private void OnGUI()
	{
		if (fadingIn || deadFadeIn)
		{
			keepBlack = false;
			alpha -= Mathf.Clamp01(Time.deltaTime / (deadFadeIn ? deadFadeTime : timeToFade));
			GUI.color = new Color(0, 0, 0, alpha);
			GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), texture );
			if (alpha <= 0)
			{
				fadingIn = false;
				deadFadeIn = false;
			}
		}
        
		if (fadingOut || deadFadeOut)
		{
			alpha += Mathf.Clamp01(Time.deltaTime / (deadFadeOut ? deadFadeTime : timeToFade));
			GUI.color = new Color(0, 0, 0, alpha);
			GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), texture );
			if (alpha >= 1)
			{
				deadFadeOut = false;
				fadingOut = false;
				keepBlack = true;
			}
		}

		if (keepBlack)
		{
			GUI.color = new Color(0, 0, 0, 1);
			GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), texture );
		}
	}
	
	
	// Use this for initialization
	void Awake ()
	{
		StartCoroutine(ChangeScene());
	}

	IEnumerator ChangeScene()
	{
		fadingIn = true;
		alpha = 1;
		yield return new WaitForSeconds(deadFadeTime);
		yield return new WaitForSeconds(timeToWait);
		fadingOut = true;
		alpha = 0;
		yield return new WaitForSeconds(deadFadeTime);
		SceneManager.LoadScene(sceneToLoad);
	}
	

	
	
}
