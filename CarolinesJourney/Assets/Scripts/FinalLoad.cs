﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalLoad : MonoBehaviour {
	public float seconds = 5f;

	// Use this for initialization
	void Start () {
		StartCoroutine (loadFinal());
	}

	private IEnumerator loadFinal() {
		yield return new WaitForSeconds (seconds);
		GameManager.instance.changeScene ("CreditScene", null);
	}
}
