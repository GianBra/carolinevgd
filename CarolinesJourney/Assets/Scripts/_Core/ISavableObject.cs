﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISavableObject{
    void Save();
    void Load();
}
