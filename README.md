# README #

This is the repository for the game "Caroline's Journey", for the course Video Game design and programming 2017.  

* CarolinesJourney: The main folder for the unity project
* Draft: a sample projects Taran did quickly at the beginning
* ai/psd/png_files: sprites for the projects to be imported in unity
* GDD.md: Game Design document
